Most recent changes:

2014-08-26
	* PgAccess gets a new home, and developer/maintainer!
	  This project hasn't been touched in ~10yrs! I think it still has
	  merit. So I'm picking it up, and furthering it's development.

02.19.2004
	* redo of 'About' window, with scrolling credits
	* updates for 0.99.0.20040219 release

02.16.2004
	* added option to connect immediately after using the new db
	  creation wizard, also other options implemented for
	  CREATE DATABASE (encoding, location, template) plus handling for
	  owner if PG >= 7.3

02.15.2004
	* added New Database Creation wizard to help walk newbies through
	  the process from Database->New

02.05.2004
	* easier importing of HTML tables with the wizard

01.04.2004
	* added notebook widget to forms, so that the notebook holds pages
	  of subforms; also improved subforms handling of variables and
	  namespaces
	* fixed import wizard when creating a new table and/or changing
	  column names from defaults (column#)

01.03.2004
	* made nice drag-n-drop for tab order traversal selection in forms
	  with BWidget ListBox

01.02.2004
	* BUG #86: resizing of Report sections now preserves offsets for
	  report objects, although functionality is different whether you
	  click the spin box or drag on the canvas, it *feels* better
	* BUG #27: list databases in "open" dialog that are available in the
	  cluster within which the current database selected in the main
	  window is a part

12.31.2003
	* added main window display for Indexes, Rules, Languages, Casts,
	  Operators, Operator Classes, Aggregates, and Conversions (should
	  be all the rest of the PG objects)
	* added "HotSyncPGA" option to "Server" menu, so anything in lib/
	  dir (except mainlib) can be edited and reloaded quickly for more
	  rapid prototyping

12.30.2003
	* added support for displaying Triggers
	* moreText - new proc for easy edit/display of text when there's
	  more than can fit into an entry widget, ie. use buttons with an
	  elipsis '...'

12.26.2003
	* create/design Domains functionality added
	* removed Domains from connections with PG < 7.3
	* made Comments work for Views

12.23.2003
	* added -nicepreview option for maximizing Report preview, or at
	  least maximizing it to 95%
	* fixed printing of %'s in Reports

12.22.2003
	* added support for displaying Domains

12.21.2003
	* add/modify base Types with new Types GUI

12.20.2003
	* put some BWidgets and icons into Functions

12.06.2003
	* added -printcmd command line option to default printing command
	  setup (overrides preferences)
	* preserve View permissions when Saving a modified version

12.04.2003
	* added new directory 'utils', and new script called mkkit.tcl that
	  generates a starkit for pgaccess -brett

12.01.2003
	* added -multipleselection option to Calendar widget, defaults to
	  letting you pick more than one date
	* fixed up user create/modify in UserGroups to use Calendar widget
	  when choosing Valid Until date

11.30.2003
	* many improvements and options added to Calendar widget

11.29.2003
	* added a Calendar megawidget as a BWidget, functionality will
	  eventually mirror the iwidget's Calendar, and hopefully get
	  included with BWidgets

11.24.2003
	* added options.tcl file, for the options db. We should migrate to
	  using this to setting globals config options, instead of
	  explicitly setting them in the widgets (where applicable). We can
	  also use the option db down the road to accomplish *basic* theming
	  (i.e. colors, fonts, etc). -brett
	* more .hlp files translated -bartusl
	* improved LaTeX parser -bartusl

11.23.2003
	* added WITH GRANT OPTION support to User-Groups window, by making
	  the permission button grey if GRANT OPTION, and fixed some quoting
	  for funky named tables and functions

11.19.2003
	* adding the html.sty file that is needed for compiling the LaTeX
	  source. -bartus
	* more .hlp files translated to .tex files -bartusl
	* worked in Checksum for Felipe Voloch's Barcodes

11.18.2003
	* putting scrollbars into 'grid' instead of 'pack' geometry manager,
	  for proper GUI design, starting with the main window and also the
	  new Types window
	* composite Types can now be created

11.17.2003
	* added preliminary support for PG Types
	* set tablelist in right-pane to use 'dictionary' sort mode, which
	  is best of 'ascii' and 'integer' sorts

11.16.2003
	* added LaTeX support to PgAccess help. Now the help files can be
	  translated to LaTeX files. This way we can easily create .dvi,
	  .pdf, .hmtl documentation for PgAccess: just 'make dvi|pdf|html'
	  in the help directory. -bartusl
	* translated and added to the repository a few help files to LaTeX,
	  for trying out this new feature. -bartusl

11.15.2003
	* added Databases, Schemas, and Languages to User-Groups

11.14.2003
	* fix for Schema qualified Views in User-Groups manager
	* Functions now show their Schema, but User-Groups GRANT needs work
	* started some Schema handling for Sequences

11.13.2003
	* Views in PG 7.3+ now have a separate Schema column

11.12.2003
	* got updated pgin.tcl for PG 7.4 and new frontend/backend protocol
	  v3, now need both pgin2.tcl and pgin3.tcl for v2/v3 protocols,
	  depending on command line option: -pgintcl for v2, and -pgintcl3
	  for v3

11.09.2003
	* cleaned up loading of Tcl-PG interface for new Pgtcl package, so
	  that should obsolete the old shared lib method in the future, as
	  fix for bug #116

10.30.2003
	* added barcodes for Reports, usage: "Barcode:: $height
	  $number_to_code"
	* added PgAccess Images to Reports, usage: "PGAImage::$imagename"

10.14.2003
	* fixed bug in export wizard, removed OID column from export proc in
	  tables namespace (bug found by Tony Grant) AND also fixed
	  off-by-one error on export

10.08.2003
	* added isTable to Database namespace
	* implemented some suggestions by Eildert Groeneveld in import
	  wizard, like a whitespace delimiter preset (and some bugs while
	  there)

10.05.2003
	* added patch from Paul J. Morris for DISTINCT values in visual qb
	* a little cleaning of Brett's work to handle table indexing issues
	  so they play nice with the visual qb

09.01.2003
	* updates for PG 7.4, mostly in Views but some Table stuff, too
	  (mostly a migration of code from mainlib to database namespace)

08.27.2003
	* cleaning out some funk in the connections management

08.04.2003
	* updated BWidgets from 1.4.1 to 1.6.0
	* several improvements to User-Groups manager thanks to better
	  BWidget Tree
	* highlighting PG/PGA objects blue/red respectively in main window

07.22.2003
	* splash screen fix by Adam Leko -bartusl
	* updated tablelist from 2.7 to 3.3

07.19.2003
	* added splash screen thanks to Adam Leko

07.18.2003
	* added Sorting and Filtering to Queries
	* debug messages hidden on startup. The -debug command line
	  parameter can be used to display them. They can be viewed even
	  runtime using the Main/Database/Debug window. -bartusl
	* command line parameters sorted on the help page. -bartusl

07.16.2003
	* sorting Views alphabetically in the main window, pretty minor

07.11.2003
	* upgraded to version 1.5.0 of L.J. Bayuk's pgin.tcl
	* bug 103, added View name to title of open View window
	* bug 106, multiple layout cleaning

07.07.2003
	* fixed 'move down' button for table design thanks to Dominique
	  Leger

07.02.2003
	* added several extra checks around opentree commands to see if node
	  that is about to be opened actually exists

06.13.2003
	* changed control buttons in lots of modules to a link relief
	* import/export wizard fixed when column names have white space

06.12.2003
	* added Gary Howell's idea for returning name of opened window for
	  Views, and extended to Tables and Queries
	* fixed whitespace issues with Views, dropped pg_get_viewdef()
	  Function for use of pg_views View
	* fixed couple quirks with User-Group Manager
	* you can now rename all objects from the main window

05.21.2003
	* multiple connections work, added Open and Delete buttons to
	  connections manager window, also more scrubbing of lint from
	  connections file

05.20.2003
	* bug 19, added the red cross back, even when making a new Diagram,
	  except when printing (it is then hidden)
	* allowing db connection selection for PgAckages
	* added PgAckages and Command Line Options to help system
	* weekly release 0.98.8.20030520 after the above 3 items

05.19.2003
	* added pseudo-table widget and automagic bindings to Forms
	* libpgtcl loading failures now fallback to using pgin.tcl
	* handy button commands for DataSet traversal in Forms
	* fixing up READMEs before tomorrow's release

05.13.2003
	* added Ctrl-f searching for Forms command code and Scripts

05.12.2003
	* fixed Ctrl-v so it pastes over stuff instead of appending
	* added extra user-defined bindings support to Form widgets

05.06.2003
	* 0.98.8.20030506 'weekly' release

04.30.2003
	* added Help icons all over the place

04.28.2003
	* added printing for Scripts, Queries, and Visual Queries

04.24.2003
	* added BWidgets and standardized printing for Diagrams

04.22.2003
	* made showError use BWidget, might allow for email reporting of
	  bugs

04.15.2003
	* icons in visual query builder, now listing views there, too
	* improved pg/pga object display from user prefs
	* added presets to import/export wizard (and some unicode)
	* fixed up Makefile, made install safer

04.11.2003
	* added exporting from Views and Queries, and reworked sql txt
	  builder

04.03.2003
	* more Usergroups enhancements - better tree and PG/PGA obj viewing

03.31.2003
	* tagged release for today, where it is yet i dunno
	* after tagging, fixed "Add New Index" button in tables.tcl

03.29.2003
	* preparing for weekly release 0.98.8.20030331

03.26.2003
	* bug 95 (part): added Ctrl-w to close _lots_ of windows
	* cleaned up Usergroups for 7.3, linked from Table perm window

03.25.2003
	* bug 95 (part): added Ctrl-q|n|c|r|x|d|o shortcuts to main window

03.24.2003
	* bug 87: added -smlogin clp to only show entry boxes on the login
	  window that were not supplied on the command line
	* bug 85: rename images now works both in design time and from the
	main form.

03.23.2003
	* changed mainlib and connections to only use new form of pg_connect
	* added -temp command line param to only make TEMP pga_* tables
	* allowing finer grain selection of objects to save in PgAckages
	* applied L.J. Bayuk's patch, the author of pgin.tcl, to
	  importexport to let COPY work with pgin.tcl

03.19.2003
	* forcing cmdline package load from widgets dir if not installed

03.18.2003
	* bug 101: exporting with COPY was broke
	* bug 100: exporting from table/view/query results implemented
	* before today, added rotation and anchor to printing postscript
	* fixed ::Database::quoteObject & brett
	* bug 62: dump should work with varying number of conn params
	* bug 97: export wizard now does some pretty nice HTML
	* added pgintcl command line option to load pgin.tcl plugin, thus
	  giving you a pure Tcl-PG interface instead of the dll/so

02.13.2003
	* bugfix: hiding systen functions
	* added return type at the functions list in main window

01.17.2003
	* preparing for weekly release, 0.98.8.20030117
	* added osx folder, fixed win docs

01.11.2003
	* finished export wizard, improvements to import wizard, no presets
	* modified TkWizard to use BWidgets, and added to it -geometry param

01.10.2003
	* added -conn and -password command line switches, for automatic
	  loading connections also specified on the command line, but
	  allowing a password to echo to the screen is still a Bad Idea and
	  should be changed
	* added -geometry tag to tkwizard, fixed up import wizard to use it

01.08.2003
	* added a popup menu to the main window objects list.
	  New/Open/Design/... can be reached by clicking with the right
	  button on the list and choosing an entry in the popup menu.

01.07.2003
	* made -noauto command line parameter prevent loading and saving of
	  connections
	* added -noscript clp to prevent execution of 'autoexec' Scripts

01.06.2003
	* added clone proc to all PGA objects and all supported PG objects,
	  for use with copying objects, and the introspect proc now just
	  calls clone with the same names for original and destination
	  objects (used to make PgAckages)
	* the PgAckage demo/demo.pga now completely holds the demo

01.04.2003
	* added login, host, dbname, pgport, and username command line
	  params
	* added a tree demo for forms as a pgackage in demo/

12.17.2002
	* more fixes for the startup procedure, not finished yet.
	* support for version numbering in the first part of the
	  pgaccess.tcl file.
	* show/hide main window support added ('hide' command line
	  parameter)

12.16.2002
	* preparing for weekly release, 0.98.8.20021216

12.15.2002
	* added BWidget Tree to forms, a subform widget, and images (almost)

12.14.2002
	* added introspect proc to tables module, tied into PgAckages
	* delete bad connection when auto-connecting to a non-pga aware db

12.13.2002
	* added SendExcel from Karl Swisher into extra directory
	* fixed delimeter bug in old import/export code, added while making
	  the wizard

12.11.2002
	* added preliminary debug support (Database menu -> Debug)

12.10.2002
	* now doing limited imports with the wizard

12.09.2002
	* moved import/export wizard to use tkwizard, almost completed
	* fixed the libpgtcl load procedure and moved it from 'main' to
	  'init'
	* preliminary suport for command line parameters added

12.07.2002
	* merged beta-2 branch back into mainline CVS

12.05.2002
	* changed startup procedure. Now all is made in tcl! Variables can
	  be set trough the config file, the enviroment variables and
	  command line parameters. Precedence levels:
	  defaults/config/env.var/cmd.line
	* code restructured and simplified

12.04.2002
	* moved Magyar translation from current CVS to this beta-2 branch
	* final touch up to docs before releasing 0.98.8

11.26.2002
	* fixed bug 70, updating documentation to point to wiki and
	  Tcl/Tk 8.3 version notice (8.3 required for BWidgets)

11.25.2002
	* cosmetic change of 'Users' tab to read 'Users/Groups'
	* Preserving OID's on saving functions. Patch submitted by Nikita
	  V. Borodikhi - Thank's!

11.22.2002
	* users/groups now handles tables, views, sequences, and functions
	* pg/pga tables now on by default, regardless of preferences, as it
	  didn't appear to be working with PG v7.2

11.15.2002
	* fixed bug 53, allow resizing of reports
	* cleaned up reports significantly
	* updated internal documentation from 'Help' on main screen
	* added more documentation to some code
	* prepared for release of 0.98.8 final

11.13.2002
	* Added patches from Stephane Mariel (French lang & intlmsg).
	* Fixed bug where toggling the global view pgaccess/system tables
	  flag did not affect the connections.
	* fixed bug where pgaccess tables were still showing up for 7.3 dbs.

11.10.2002
	* preliminary addition of new users and groups window, which at
	  least provides the same level of support for user modifications,
	  and adds a little support for groups (there's much more to do!)

11.04.2002
	* "Add new column" window redesigned (Tables info)

11.01.2002
	* "Open database" window redesigned

10.25.2002
	* Images load, view, modify features added, plus with the
	  "Images::get $imagename" one can get the base64 code of the image
	  (it can be used to show the image on the forms)

10.17.2002
	* images.tcl module added. It is just a framework yet with no
	  functionality.

10.13.2002
	* preliminary add of import-export wizard, doesn't work yet though

10.11.2002
	* the "Table information" window redesigned using the tablelist
	  widget
	* added intlmsg error message (stdout) if untranslated message found

10.05.2002
	* added introspect proc to all PGA objects
	* expanded PgAckages to all PGA objects

10.04.2002
	* fixed bug #60 - table INSERT, UPDATE, DELETE in tables module for
	  PG 7.3
     
	* added support for copying PGA objects across database connections
	* added preliminary support for PgAckages, just Graphs right now

10.03.2002
	* Added better checking/reporting to shared lib loading... still
	  needs some work though.
	* re-worked ::Database::quoteObject to fix bug and stream line a
	  bit.
	* 0.98.8 beta 2 !!!
	* now begins the joys of multiple branches in CVS :)

10.02.2002
	* fixed bug 51, added Save As command to Scripts, Forms, Reports, in
	  addition to making Save command use OIDs to refresh itself
	* added check for empty lastdb on initial connection
	* moved some SQL from tables to database module for 7.3 table design
	* finished graphs, now can save and print

10.01.2002
	* Added initial support for per connection preferences (manage
	  connections)
	* fixed bug 49, relating to proper display of object class in form
	  design
     
09.27.2002
	* fixed bug where copy, rename, delete would get invoked for host
	  nodes in the tree.

09.26.2002
	* fixed bug #57. Instead of 64 chars the procedure uses 32 chars
	  from the column definition. It is just a temporary solution, we
	  should replace any table view control with the tablelist
	  component.

09.23.2002
	* removed code that delete connection info if autoload=false.
	* added pgaccesstables preference to show/hide pgaccess tables
	  (bug 48)
	* added back the lastdb rememberence code... lastdb selected in
	  tree.
	* added new global procedure in the pgaccess.tcl file for debug
	  purposes say $msg just opens a tk_messageBox with the given
	  message
	* renamed all schema references to diagram
	* added primary keys on pga object names to fix bug #36

09.22.2002
	* moved more connection stuff from mainlib to connections namespace,
	  should be handling add/remove of connections with/without
	  passwords and auto-loading and saving passwords with more
	  stability

09.20.2002
	* added tool tips to form icon buttons, couple other form bugs
	* fixed script cache, now actually works (looked at SQL window)

09.18.2002
	* added todo list
	* added bugs list
	* syntax highlight bugfix for switches highlight ( -xyz )
	* fixed up demo, primarily using demo 7.2 version

09.16.2002
	* added initial Graphs module (from Bret Green's code)
	* working on password connections handling
	* added ::Database::getColumnsList and ::Database::quoteObject

09.13.2002
	* Added Connections::openConn proc to replace open_database on
	  startup, to hopefully fix the problems with pgaccess crashing on
	  startup.

09.10.2002
	* added cacheing to Scripts, also cleaned scripts up
	* added Bret Green's graphing code to extra/ dir

09.09.2002
	* 0.98.8 beta 1 !!!

09.08.2002
	* added BWidget's SelectFont to forms

09.07.2002
	* fixed bug where sys tables were not being displayed, when show sys
	  tables flag was on.
	* changed tables.tcl so that it would handle schema.table properly
	  when display table contents.
	* changed get_dwlb_selection so that it returns schema.table for
	  Tables, and version >= 7.3.
	* added checks to pgaccess.tcl to check that dbnames are not empty
	  in connections file.
	* added selection of db node upon startup.
	* added ::Connections::getIdFromHandle proc to connections.
	* minor touch up to reports, columns were screwing up
	* added win32 instructions and README.win32 (straight from the wiki)
	* added icons to forms and reports, plus a couple of tweaks to both

09.06.2002
	* fixed bug where the last db closed would cause an error.
	* renamed external names of "schema" to "diagrams".
	* moved pg-new.tcl to pgaccess.tcl and mainlib-new.tcl to
	  mainlib.tcl. New interface is in place.
	* created a tag for pgaccess.tcl and mainlib.tcl before adding the
	  new interface tag = old-interface in case we need to refer back to
	  the old interface.
	* added window geometry to preference file, so that the last session
	  window size is remembered. This works for both "Exit" and if the
	  window is destroyed.
	* copied (from pgaccess.tcl) and fixed startup procedure in
	  pg-new.tcl
	* removed PGACCESS_SxHg enviroment variable. Syntax highlight is
	  enabled all the time.

09.05.2002
	* reports get all recs per page at once, except for Functions

09.02.2002
	* added support for mulitple connections. This affected Mainlib
	  mostly, and pg-new.tcl.
	* added connections namespace to support the connection info for
	  storing the multiple connection data. There are several helper
	  procs now for connections.
	* changed icons a little bit.
	* added addHostNode and addDbNode procs to add nodes to the tree
	  listing.
	* added qualifySysTable proc so that you can wrap system table names
	  in this proc and it will add the proper schema (Pg_catalog) if the
	  version is >= 7.3.
	* changed how PG version is retrieved. It is now stored in the
	  connection array after intial retrieval from DB.
	* cleaning forms and reports to better comply with developer tidbits
	* added BWidget SelectFont to reports, ComboBox for table/view list
	* added "Update" and "Edit" buttons for labels and formulas in
	  reports

09.01.2002
	* fixed the main tablelist, so that for integers, it sorts based on
	  integer. Before it was just an ASCII sort.
	* handleToolBar proc to handle the "show toolbar" flag.
	* Capitalized "Access" in window title.
	* updated toolbar. There are 2 toolbars now. One for "Database" and
	  one for "Object".
	* added new icon library icons.
	* fixed some of the button commands in the toolbar.
	* added scrollbars to the tree view.
	* added "decrypt" icon. This needs more functionality put in though.
	* changed font for main tablelist to use the font_normal global
	  default font.
	* used combobox for host in open db window, and used spinbox for
	  port number.
	* added notebook to the preference window.
	* change from pack to grid geometry manager.
	* added font selectors for the fonts in the preference window.
	* added "show toolbar" checkbutton.
	* used spinbox for "max rows...".
	* used combobox for "language".
	* changed how the preferences were loaded.
	* moved -command data for the save button into its own proc
	  (initSave).
	* changed the default fonts to the "new way" of doing fonts in Tk...
	  although it really isn't new.
	* used icons for the table rows view.
	* cleaning up "update dataset vars" functionality in forms

08.31.2002
	* forms: fixed bug 39, added isloaded function, fixed demo cursors
	* added combobox to forms, for both small apps and core use
	* added validation commands to entry boxes
	* added "update dataset vars" command to pseudo-query widgets

08.30.2002
 	* added bindings to Scripts edit window:
		<Escape> to close the window
		<Control-Key-s> to save script
 	* the same for Functions. And another thing: on save, the functions
 	  window will not be closed anymore. It was quite annoying.
 	* needed a little work on views for them to work with 7.3.

08.29.2002
 	* added scrollbars to the text widget in scripts design
 	* sql/plpgsql syntax highlight fixes for $... variables

08.28.2002
 	* bugfix for tcl syntax highlight at quoted string highlight: now
 	  handling multiple line strings and escaped quotes.

08.27.2002
 	* cleaning up the pgaccess.tcl, and fixing the startup process.
 	* added getSQL, setVars, and modified setSQL for DataControl in
 	  forms so that forms can now work with stored queries more easily,
 	  also had to modify loadQuery and getSQL in queries to be visual or
 	  not.
 	* moving third party widgets to widgets directory.
 	* improved pgmonitor standalone capability (standalone flag).
 	* changed minimum size of widgets in forms upon creation to twice
 	  the grid size in either direction.

08.26.2002
 	* added hostname to Pgmonitor window title.
 	* more changelog cleanup.
 	* form widget resize adjustment.
 	* form set name of widgets was a little buggy.

08.25.2002
 	* moved Pgmonitor init so that it gets init when first invoked.
 	* took out exits in Pgmonitor so that they don't kill the whole app
 	  when a error occurs.
 	* fixed password saving.
 	* added Server menu tab to the right of Object, with dump and pgmon.
 	* added getSQL command to Queries for returning text of them.
 	* moved ImportExport into its own namespace.
 	* allow form widget resizing with mouse.

08.24.2002
 	* forms: fixed bug 33 - form designing/testing of multiple forms.
 	* forms: fixed bug 35 - escaping of \s for capitalized tables.
 	* forms: removed 8k limit.
 	* demo: fixed report, touched up phonebook form a little.
 	* put pgmonitor into Database menu, below dump database, and removed
 	  pgaccess file from repository.
 	* changed Makefile to respect this new pgaccess.tcl format.
 	* PGACCESS_HOME, when created in pgaccess.env by pgaccess.tcl, set
 	  to /usr/lib/pgaccess by default.
 	* allow for saving of passwords from preferences checkbox.
 	* changed pref,print_file to print,print_file var name.

08.22.2002
 	* integrated PGMonitor into PGAccess. There is a new menu item
 	  called Plugins where you can invoke PGMonitor.
 	* Added other widgets. Initially, these are to support PGMonitor,
 	  but they can start being used for others as well. See the BWidgets
 	  and tablelist directories.
 	* added the close_database proc.

08.22.2002
 	* modified startup procedure, the rc settings now are loaded from
 	  the ~/.pgaccess/pgaccessrc file.
 	* main.tcl renamed to pgaccess.tcl, and the linux/unix startup
 	  script is added to it.
 	* added dump database functionality.
 	* inverted the changelog file, this way is more visible.

08.21.2002
 	* added syntax highlight procedure for sql/plpgsql.
 	* syntax higlight for functions, queries, views.

08.14.2002
 	* tables, scripts, functions, help files: 7.3 changes, NAMEDATALEN
 	  to 64.

08.13.2002
 	* forms.tcl: made test form/close test form one button, little bugs.
 	* tpe.tcl: Teo's Tcl Project Editor added to the repository.
 	* forms.tcl: added snap to grid, fixed some anchor/justify.

08.11.2002
 	* mainlib.tcl: switch imp/exp from tcl reads to gets.

08.09.2002
 	* mainlib.tcl: tried to clean Import/Export some more.
 	* tables.tcl: added Brett's patch to allow mixed-case column sorts.
 	* forms.tcl: added spinboxes, form bgcolor, cursors, fixed tabsort,
 	  added grid to design, added ability to change class of item, lots
 	  of stuff.

08.07.2002
 	* reports.tcl: images can now be pulled from the database.

08.06.2002
 	* tables.tcl, scripts.tcl: made column sorts double-click mouse
 	  button 1, made syntax highlighting refresh upon save.
 	* mainlib.tcl: added copying for several sections.
 	* mainlib.tcl: trying to clean Import/Export to/from tables/files.
 	* forms.tcl: justifying in addition to anchoring from earlier today.

08.05.2002
 	* tables.tcl: applied Brett's patch for column sort on mouse click.
 	* forms.tcl: aligning for labels, entry, btns, checks, rads in
 	  forms.
 	* scripts.tcl, forms.tcl: checking for valid tcl upon saving.
 	* printer.tcl: output now Postscript, Text, HTML, and its using
 	  pack.

08.03.2002
 	* printer.tcl: allows print callbacks to be a list of Tcl functions.
 	* forms.tcl, scripts.tcl: added cut, copy, paste with Ctrl-x|c|v for
 	  Tcl code per bug 20.
 	* mainlib.tcl: added John Turner's changes to About box.

08.02.2002
 	* forms.tcl, scripts.tcl: added Bartus' syntax highlighting.

07.31.2002
 	* mainlib.tcl: modified import/export to work like psql \copy cmd.

07.30.2002 (after weekly release 2)
 	* printer.tcl, reports.tcl: new interface for printing to multiple
 	  formats from tk canvas widgets.

07.30.2002
 	* mainlib.tcl: added Help button for open database.

07.29.2002
 	* main.tcl: modified showError to take an optional help topic.
 	* preferences.tcl, printer.tcl, reports.tcl: added print dialog
 	  and default print to command, modified reports to use it.

07.26.2002
 	* added syntax higlight for tcl code. Right now it works only for
 	  pltcl functions. No refresh yet!!! (developement version). One can
 	  control the existence using the PGACCESS_SxHg enviroment variable.
 	* added new module that implements a siple Stack mechanism
 	  .(stack.tcl)

07.25.2002
 	* forms.tcl: added brett schwarz's patch to make pasting nicer.
 	* forms.tcl: added cut, copy, paste with Ctrl-x|c|v in form design.
 	* mainlib.tcl, database.tcl, reports.tcl: added getViewsList so
 	  views are visible again and not returned with getTablesList.

07.23.2002
 	* reports.tcl: added multiple columns.
 	* reports.tcl: fixed postscript output wrapping of encapsulated ps.
 	* reports.tcl: added support for using Functions in reports.
 	* reports.tcl: added GUI for grouping/sorting, little functionality.

07.15.2002
 	* reports.tcl: no longer allowing empty report names.
 	* mainlib.tcl: now refreshing the reports list when you delete one.
 	* tables.tcl: fixed "ERROR:  Attribute 'oid' not found" on opening
 	  existing table design.

07.13.2002
 	* schema.tcl: added coordinate system. now you can do schemas bigger
 	  then your screen and save them.
 	* schema.tcl, mainlib.tcl: on rename table or rename field schema
 	  names are changed to.
 	* reports.tcl: corrections for when you pick an invalid table or
 	  view.
 	* reports.tcl and forms.tcl: corrections to bind of Destroy, now
 	  makes sure you are destroying the parent and not a listbox.

07.12.2002
 	* pgaccess.sh: moved PGLIB to /usr/lib.
 	* reports.tcl: added browsing for pictures.
 	* reports.tcl: added 'where / order by' sql entry.
 	* database.tcl: allowed getTablesList to retrieve Views as well so
 	  the reports can now use views and not just tables.
 	* reports.tcl and forms.tcl: now binding Destroy of one window to
 	  close of all the windows (except report preview and test form).

07.11.2002
 	* modified main.tcl for a better startup procedure. I think this
 	  modifications initially were made by the Debian team, I just
 	  copied them.
 	* the same for the pgaccess.sh.
 	* created a copy a pgaccess.sh in pgaccess to let people start the
 	  program typing just pgaccess. It's not just a symbolic link to
 	  prevent overwriting by "make all".
 	* functions.tcl: bugfix for working with overloaded functions, and
 	  another one: if somebody has made a mistake in the editing
 	  process, the function was deleted, and recreation was not
 	  possibble, because the program returned an error message. Now the
 	  error message is still persists, but the procedure doesn't take
 	  this into consideration.
 	* functions.tcl: Added "Save as" button to create a new function
 	  with the same source. Very usefull when one needs a new function
 	  with slight modification to the source code.
 	* functions.tcl(Functions window): The default window size is
 	  increased to let larger source code visible without resizing.
 	* mainlib.tcl(main window): for the same bugfix (overloading), now
 	  the functionnames are represented with the parameters too, I
 	  think, that the structure is more visible now. Bugfix for the
 	  introduced functionalities, at deleteing object (function).
 	* added chinene and chinese_big languages (made by ???).

07.10.2002
 	* added validation to form names to prevent first letter being Upper
 	  Case.
 	* added tab order / keyboard traversal selection to forms.
 	* major work to reports, added pictures, multiple pages.

********************************************************************
The pgaccess team.
